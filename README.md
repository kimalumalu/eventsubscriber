# README #

This is a WordPress plugin for managing subscriptions to event generated and handled by "events manager plugin" (see more at: http://wp-events-plugin.com)

### What is this repository for? ###

* This is a WordPress plugin for managing event subscriptions
* Version 1.0

### How do I get set up? ###
* Prequisites: latest docker and docker-compose libraries are available on your workstation
* checkout git repository https://bitbucket.org/kimalumalu/wordpresschoirevent
* cd to checkout directory
* run docker-compose up -d
* login to your wordPress website available at: http://localhost:8000
* download and set up "events manager plugin" see description at: http://wp-events-plugin.com/documentation/
* Create event locations, categories and items
* Activate "events subscriber" plugin on WP Dashboard
* Access event subscription admin page

### Who do I talk to? ###

* Zolana Nsakala: nzolana@googlemail.com