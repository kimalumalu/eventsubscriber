<?php
// Do not allow direct access!
if ( ! defined( 'ABSPATH' ) ) {
    die( 'Forbidden' );
}
/**
 * This is a helper responsible of fetching, caching, querying and persisting
 * event subscription items
 */
class Event_Subscription_Manager {

    /**
     * Instance of event manager subscribe
     * 
     * @param Event_Subscription_Manager 
     */
    private static $instance;
    
    /**
     * List of all existing user names
     * 
     * @param array $users
     */
    private $users = array();
    
    /**
     * Current user
     * 
     * @var array
     */
    private $current_user = array();
    
    /**
     * List of all events grouped by existing event categories
     * 
     * @param array events_grouped_by_categories
     */
    private $events_grouped_by_categories = array();
    
    /**
     * List of all existing event subscriptions
     * 
     * @param array 
     */
    private $event_subscriptions = array();
    
    /**
     * Retrieves list of existing subscription status
     * 
     * @param array
     */
    private $subscription_status_values = array(
        'STATUS_PRESENT',
        'STATUS_ABSENT',
        'STATUS_MAY_BE'
    );
    
    /**
     * retrieves an instance of Choir_Event_Manager_Subscribe
     * 
     * @return Choir_Event_Manager_Subscribe
     */
    public static function instance() 
    {
        if( is_null( self::$instance ) ){
            self::$instance = new self();
        }
        
        self::$instance->init();
        
        return self::$instance;
    }
    
    /**
     * Renders view
     * 
     * @example $this->get_subscription_status_values()
     * @example $this->get_event_categories()
     * @example $this->get_event_items('Konzert')
     * @example $this->get_event_item('Konzert', '14')) 
     */
    public function render() 
    {
        $this->view( 'templates/subscription' );
    }
    
    /**
     * Retrieves relevant data of current user
     * 
     * @return array
     */
    public function get_current_user()
    {
        return $this->current_user;
    }        

    /**
     * Retrieves list of all existing users
     * 
     * @return array
     */
    public function get_users() 
    {
        return $this->users;
    }

    /**
     * Retrieves list of existing subscription values
     * 
     * @return array
     */
    public function get_subscription_status_values()
    {
        return $this->subscription_status_values;
    }

        /**
     * Retrieves list of existing event categories
     * 
     * @return array
     */
    public function get_event_categories()
    {        
        return array_keys( $this->events_grouped_by_categories );
    }
    
    /**
     * Retrieves list of event matching to given specific category
     *  
     * @param string $category
     * 
     * @return array
     */
    public function get_event_items( $category ) 
    {
        $event_items = array();
        
        if ( isset( $this->events_grouped_by_categories[ $category ] ) ) {
            $event_items = $this->events_grouped_by_categories[ $category ];
        }
        
        return $event_items;
    }
    
    /**
     * Retrieves persited event name mapped to given event category
     * 
     * @param string $category
     * 
     * @return array
     */
    public function get_event_names( $category ) 
    {
        $event_names = array();
        $event_items = $this->get_event_items( $category );
        
        foreach ( $event_items as $event_item ) {   
            if ( is_array( $event_item ) && array_key_exists( 'post_title', $event_item ) ) {
                 $event_names[ $event_item[ 'ID' ] ] = $event_item[ 'post_title' ];
            }
        }
        
        return $event_names;
    }            

    /**
     * Retrieves event item matching category and post id
     * 
     * @param string $category
     * @param string $post_id
     * 
     * @return array
     */
    public function get_event_item( $category, $post_id ) 
    {
        $event_item  = array();        
      
        if ( isset( $this->events_grouped_by_categories[ $category ][ $post_id ] ) ) {
            $event_item = $this->events_grouped_by_categories[ $category ][ $post_id ];
            $event_item[ 'location' ] = $this->get_event_location( $post_id );
            $event_item[ 'subscriptions' ] = $this->get_event_subscription_data( $post_id );
        }
        
        return $event_item;
    }
    
    /**
     * Subscribe current user to given parent post id
     *
     * @param string $subscription_status
     * @param string $parent_post_id
     */
    public function subscribe_to_event( $subscription_status , $parent_post_id ) 
    {     
        $current_user_subscription = $this->get_current_user_subscription( $parent_post_id );
        
        if ( empty( $current_user_subscription ) ) {
            $subscription_post = array(
              'post_content' => $subscription_status,
              'post_status' => 'publish',
              'post_type' => 'event_subscription',
              'post_parent' => $parent_post_id
            );
            wp_insert_post( $subscription_post );            
        } else {
            $current_user_subscription[ 'post_content' ] = $subscription_status;
            wp_update_post( $current_user_subscription );
        }
    } 
    
    /**
     * Initializes all required data containers/lists
     */
    private function init() 
    {
        $this->init_current_user();
        $this->init_users();
        $this->init_event_subscriptions();
        $this->init_events_grouped_by_categories();
    }
    
    /**
     * Initializes relevant data of current user
     */
    private function init_current_user()
    {
        $current_user = wp_get_current_user();
        
        $this->current_user[ 'user_id' ] = $current_user->ID;
        $this->current_user[ 'user_name' ] = $current_user->display_name;
    }        

    /**
     * Initializes list of all existing users 
     */
    private function init_users()
    {
        $users = get_users( array( 'fields' => array( 'ID', 'display_name' ) ) );
        
        foreach ( $users as $userObject ) {
                $this->users[ $userObject->ID ] = $userObject->display_name;
            }
        }  
    
    /**
     * Initializes list of existing events grouped by categories
     */    
    private function init_events_grouped_by_categories() 
    { 
        $taxonomies = array( 
            'event-categories',
        );

        $args = array(
            'orderby'   => 'name', 
            'order'     => 'ASC',
            'hide_empty'=> true
        ); 

        $terms = get_terms( $taxonomies, $args );
        
        foreach ( $terms as $term ) {
            $event_post_data = get_posts(
                array(
                    'post_type' => 'event',
                    'tax_query' => array(
                        array(
                        'taxonomy' => $term->taxonomy,
                        'field' => 'slug',
                        'terms' => $term->slug,
                        )
                    )
                )
            );
        
            $this->events_grouped_by_categories[ $term->name ] = $this->get_reduced_event_post_data(
                 $event_post_data
            );        
        }
    } 
    
    /**
     * Initializes list of existing event subscriptions
     */
    private function init_event_subscriptions() 
    {
        $args = array(
            'post_type' => 'event_subscription',
            'numberposts' => '-1'
        );

        $event_subscriptions = get_posts( $args );
        
        foreach( $event_subscriptions as $subscription_object ) {
            $this->event_subscriptions[ $subscription_object->ID ] = array(
                'post_author' => $subscription_object->post_author,
                'post_date' => $subscription_object->post_date,
                'post_content' => $subscription_object->post_content,
                'post_parent' => $subscription_object->post_parent
            );
        }
    }
    
    /**
     * Retrieves list of event post data with focus on specific fields (ID,title,etc)
     * 
     * @param array $post_data
     * 
     * @return type
     */
    private function get_reduced_event_post_data( array $post_data ) 
    {
        $reduced_event_post_data = array();
        
        foreach ( $post_data as $post_object ) {
            $reduced_event_post_data[ $post_object->ID ] = array_merge(
                array(
                    'ID' => $post_object->ID,
                    'post_author' => $post_object->post_author,
                    'post_title' => $post_object->post_title,
                    'post_date' => $post_object->post_date
                ),
                $this->get_event_date_and_time_data( $post_object->ID )
            );
        }
        
        return $reduced_event_post_data;        
    }
    
    /**
     * Retrieves current user subscription if existing
     * 
     * @param string $post_id
     * 
     * @return array
     */
    private function get_current_user_subscription( $post_id )
    {
        $current_user_subscription = array();
        
        foreach ( $this->event_subscriptions as $id => $subscription ) {
            if ( 
                 $subscription[ 'post_author' ] == $this->current_user[ 'user_id' ] 
                    &&
                 $subscription[ 'post_parent' ] == $post_id
            ) {
               $current_user_subscription = $subscription; 
               $current_user_subscription[ 'ID' ] = $id;
               $current_user_subscription[ 'post_type' ] = 'event_subscription';
               break;
            }
        }
        
        return $current_user_subscription;        
    }
    
    /**
     * Retrieves location where event identified by post_id will take place
     *
     * @param string $post_id
     * 
     * @return array
     */
    private function get_event_location( $post_id )
    {
        global $wpdb;        
    
        $query = sprintf(
            'SELECT '.
            'wee.post_id ,'.
            'location_name ,'.
            'location_address ,'.
            'location_town '.                
            'FROM %sem_locations wel '.
            'LEFT JOIN %sem_events wee '.
            'ON(wel.location_id = wee.location_id) '.
            'WHERE wee.post_id = %s',
             $wpdb->prefix,
             $wpdb->prefix,
             $post_id
        );
        
        $result = $wpdb->get_results( $query );
        
        $event_location_db_entry = array_pop( $result );
        
        $event_location_data = json_decode(
            json_encode( $event_location_db_entry ),
            true
        );
        
        return $event_location_data;
    }
    
    /**
     * Retrieves event date and time data matching to given post_id
     * 
     * @param string $post_id
     * 
     * @return array
     */
    private function get_event_date_and_time_data( $post_id ) 
    {
        global $wpdb;
        
        $query = sprintf(
            'SELECT '.
            'event_start_date ,'.
            'event_end_date ,'.
            'event_start_time ,'.
            'event_end_time '.                
            'FROM %sem_events '.
            'WHERE post_id = %s',
             $wpdb->prefix,
             $post_id
        );
        
        $result = $wpdb->get_results( $query );
        $event_date_time_db_entry = array_pop( $result );
        
        $event_date_and_time_data = json_decode(
            json_encode( $event_date_time_db_entry ),
            true
        );
        
        $start_date = strtotime($event_date_and_time_data[ 'event_start_date' ]);
        $event_date_and_time_data[ 'event_start_date' ] = date('d.m.Y', $start_date);
        
        $start_time = strtotime($event_date_and_time_data[ 'event_start_time' ]);
        $event_date_and_time_data[ 'event_start_time' ] = date('H:i', $start_time);
        
        $end_time = strtotime($event_date_and_time_data[ 'event_end_time' ]);
        $event_date_and_time_data[ 'event_end_time' ] = date('H:i', $end_time);
        
        return $event_date_and_time_data;
    }
    
    /**
     * Retrieves relevant subscription data matching given post id
     * 
     * @param string $post_id
     * 
     * @return array
     */
    private function get_event_subscription_data( $post_id )
    {
        $event_subscriptions_by_post_id = array();
        
        foreach ($this->event_subscriptions as $subscription_data) {
            if( $subscription_data[ 'post_parent' ] == $post_id ){
                $user_id = $subscription_data[ 'post_author' ];
                $event_subscriptions_by_post_id[$user_id] = array(
                    'subscription_target_post_id' => $post_id,
                    'subscription_author' => $this->users[$user_id],
                    'subscription_date' => $subscription_data[ 'post_date' ],
                    'subscription_status'=> $subscription_data[ 'post_content' ]
                );   
            }            
        }
        
        return $event_subscriptions_by_post_id;
    }
    
    /**
     * Includes and renders view identified by name
     * 
     * @param string $name
     */
    private function view( $name ) 
    {
        include (
            sprintf( '%s../views/%s.php', plugin_dir_path(__FILE__), $name )
        );
    }
} 


