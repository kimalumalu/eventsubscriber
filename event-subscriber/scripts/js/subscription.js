(function($) {	
 init();
 function init(){  
      $( '#esEventCategory' ).change( function(){
         $.ajax({ 
           method: "GET",
           url:ajaxurl,
           data: {
               'action': 'subscription_ajax_handler_action',
               'request-key':'fetch-event-names',
               'category': $( this ).val()
           }
          })
          .done( function( data ){
              var options = JSON.parse( data );
              resetAllValues();
              $.each( options, function( id, value ){
                  $( '#esEventName' )
                    .append(
                        "<option value='"+id +"'>"+ value +"</option>"
                    );
              });
          });
      });
      
      $( '#esEventName' ).change( function(){
         $.ajax({ 
           method: "GET",
           url:ajaxurl,
           data: {
               'action': 'subscription_ajax_handler_action',
               'request-key':'fetch-event-item',
               'category': $( '#esEventCategory' ).val(),
               'item-id': $( this ).val()
           }
          })
          .done( function( data ){
              var eventItem = JSON.parse( data );
      
              $( '.ui-es-event-item-name' )
                 .find( '.ui-es-element' )
                 .find( 'span' )
                 .html( eventItem.post_title );
         
               $( '.ui-es-event-item-location' )
                 .find( '.ui-es-element' )
                 .find( 'span' )
                 .html( 
                     eventItem.location.location_name+' - '+
                     eventItem.location.location_address+' - '+
                     eventItem.location.location_town
                 );         
         
               $( '.ui-es-event-item-date' )
                 .find( '.ui-es-element' )
                 .find( 'span' )
                 .html( 
                     eventItem.event_start_date+' '+
                     eventItem.event_start_time+' - '+
                     eventItem.event_end_time
                 );         
          
                if( $.isEmptyObject( eventItem.subscriptions ) ) {
                    $( '#currentUserStatus' ).val( 'default' );
                    $( '.ui-es-event-attendee' ).each(function(){
                        $(this).find( '.ui-es-element span' ).text( ' - ' );
                    });
                } else {
                    var currentUserStatus = 'default'
                    $.each( eventItem.subscriptions, function( userId, subscription ){
                        $( "div[user_id='"+ userId +"']" )
                            .find('.ui-es-element span')
                            .text(
                                getStatusValue(
                                    subscription.subscription_status
                                )
                            );
                                        
                        if ( userId == $( '#currentUser' ).attr( 'data-user-id' ) ) {
                            currentUserStatus = subscription.subscription_status;         
                        }                 
                    });                    
                    $( '#currentUserStatus' ).val( currentUserStatus );  
                }
            $('.ui-es-attendee-container, .ui-es-event-item, .ui-es-user-status-container, .ui-es-submit-container').show();
          });
      }); 
      
      $( '#userStatusUpdater' ).click(function(){
          $.ajax({ 
            method: "POST",
            url:ajaxurl,
            data: {
                'action': 'subscription_ajax_handler_action',
                'request-key':'update-user-status',
                'status': $( '#currentUserStatus' ).val(),
                'item-id': $( '#esEventName' ).val() 
            }
          });         
      });
 }
 
 function getStatusValue( status ) {
     var statusValue = ' - ';
     
     switch ( status ) {
         case 'STATUS_PRESENT':
               statusValue = 'Anwesend';
               break;
               
         case 'STATUS_MAY_BE':
               statusValue = 'Vielleicht';
               break;   
           
         
        case 'STATUS_ABSENT':
               statusValue = 'Abwesend';
               break;   
               
         default:
               statusValue = ' - ';
               break;            
     }
     
     return statusValue;
 } 
 
 function resetAllValues(){
    $('.ui-es-attendee-container, .ui-es-event-item, .ui-es-user-status-container, .ui-es-submit-container').hide();
    $( '#esEventName' ).find( "option[value!='default']" ).remove();
    $( '.ui-es-event-item-name' )
       .find( '.ui-es-element' )
       .find( 'span' )
       .html( 'Event Name ' );
         
    $( '.ui-es-event-item-location' )
      .find( '.ui-es-element' )
      .find( 'span' )
      .html( 'Event Ort' );         
         
    $( '.ui-es-event-item-date' )
      .find( '.ui-es-element' )
      .find( 'span' )
      .html( 'Event Date' ); 
          
    $( '.ui-es-event-attendee' ).each(function(){
        $(this).find( '.ui-es-element span' ).text( ' - ' );
    }); 
    
    $( '#currentUserStatus' ).val( 'default' );  
 }
})( jQuery );
