<div class="ui-es-widget">
    <div class="ui-es-filter-container">
        <div class="ui-es-pair-container ui-es-event-category">
            <div class="ui-es-label">
                <label>Kategorie:</label>
            </div>
            <div class="ui-es-element">
                <select name="es_event_category" id="esEventCategory">
                    <option value="default" selected="selected">Kategorie wählen</option>
                    <?php
                        $categories = $this->get_event_categories();
                        foreach ( $categories as $category ) {
                            echo sprintf(
                                '<option value="%s">%s</option>',
                                $category,
                                $category                                    
                            );
                        }                        
                    ?>
                </select>
            </div>
        </div>
        <div class="ui-es-pair-container ui-es-event-name">
            <div class="ui-es-label">
                <label>Name:</label>
            </div>
            <div class="ui-es-element">
                <select name="es_event_name" id="esEventName">
                    <option value="default">Name wählen</option>
                </select>
        </div>                
    </div>
    </div>
    <div class="ui-es-event-item">
        <hr>
        <div class="ui-es-section-headline">Veranstaltung</div>
        <div class="ui-es-section-content">
            <div class="ui-es-pair-container ui-es-event-item-name">
                <div class="ui-es-label">
                    <label>Name:</label>
                </div>
                <div class="ui-es-element">
                    <span>Event Name</span>
                </div>  
            </div>  
            <div class="ui-es-pair-container ui-es-event-item-location">
                <div class="ui-es-label">
                    <label>Wo:</label>
                </div>
                <div class="ui-es-element">
                    <span>Event Ort</span>
                </div> 
            </div> 
            <div class="ui-es-pair-container ui-es-event-item-date">
                <div class="ui-es-label">
                    <label>Wann:</label>
                </div>
                <div class="ui-es-element">
                    <span>Event Date</span>
                </div>                      
            </div>
    </div>
    </div>
    <div class="ui-es-attendee-container">
        <hr>
        <div class="ui-es-section-headline">Teilnehmer Status</div>
        <div class="ui-es-section-content">
            <?php 
               $users = $this->get_users();     
               $current_user = $this->get_current_user();
               
               foreach ( $users as $user_id => $user_name ) {
                   if ( $user_id != $current_user[ 'user_id' ] ) {
                        echo sprintf(
                           '
                            <div class="ui-es-pair-container ui-es-event-attendee" user_id="%s">
                                <div class="ui-es-label">
                                    <label>%s:</label>
                                </div>
                                <div class="ui-es-element">
                                    <span> - </span>
                                </div> 
                             </div> 
                           ',
                           $user_id,
                           $user_name     
                        );                       
                   }
               }
            ?>                
        </div>                    
    </div>
    <div class="ui-es-user-status-container">
        <hr>
        <div class='ui-es-user-data'>
            <?php
               $current_user = $this->get_current_user();
               echo sprintf(
                 '<input id="currentUser" type="hidden" data-user-id="%s" data-user-name="%s">',
                 $current_user[ 'user_id' ],
                 $current_user[ 'user_name' ]      
               );
            ?>            
        </div>
        <div class="ui-es-pair-container ui-es-user-status">
            <div class="ui-es-label">
                <label>Dein Status:</label>
            </div>
            <div class="ui-es-element">
                <select id="currentUserStatus">
                    <option value="default">wähle deinen Status aus</option>
                    <option value="STATUS_ABSENT">Abwesend</option>
                    <option value="STATUS_PRESENT">Anwesend</option>
                    <option value="STATUS_MAY_BE">Vielleicht</option>
                </select>
        </div>               
    </div>
    </div>
    <div class="ui-es-submit-container">
        <button class="ui-es-submit" id="userStatusUpdater">Senden</button>
    </div>
</div>
